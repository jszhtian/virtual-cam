#include "pch.h"
#include "OpenCVProvider.h"
OpenCVProvider* OpenCVProvider::m_Handle = nullptr;

OpenCVProvider::OpenCVProvider()
{
	this->init();
}


bool OpenCVProvider::init()
{
	this->m_mtxlock = new std::mutex;
	this->m_frameBuffer = nullptr;
	this->m_mtx = false;
	char path[] = "C:/TMP/test.avi";
	this->m_cap.open(path);
	//this->m_cap.open(0);
	//this->m_cap->set(16, true); //convert BGR2RGB
	if (!this->m_cap.isOpened())
	{
		//MessageBoxW(NULL, L"Capture Error", L"WARNING", MB_OK);
		return false;
	}
	return true;
}

bool OpenCVProvider::lock()
{
	if(this->m_mtx==false)
	{
		this->m_mtx = true;
		this->m_frameBuffer = new char[m_bitDepth * m_height * m_width];
		return true;
	}
	return false;
}

bool OpenCVProvider::release()
{
	if(this->m_mtx==true)
	{
		this->m_mtx=false;
		delete[] this->m_frameBuffer;
		this->m_frameBuffer = nullptr;
		return true;
	}
	return false;
	
}

bool OpenCVProvider::uninit()
{
	if (this->m_mtxlock != nullptr)
	{
		delete this->m_mtxlock;
		this->m_mtxlock = nullptr;
	}

	if (this->m_frameBuffer != nullptr)
	{
		delete[] this->m_frameBuffer;
		this->m_frameBuffer = nullptr;
	}
	if (this->m_cap.isOpened())
	{
		m_cap.release();
	}
	return true;
}

char* OpenCVProvider::get()
{
	char* ret=NULL;
	if(this->m_mtx==false)
	{
		return NULL;
	}
	
	this->m_mtxlock->lock();
	//add capture code
	if(m_cap.isOpened())
	{
		cv::Mat t_frame;
		cv::Mat resizeFrame;
		cv::Size dstsize(1920, 1080);
		m_cap.read(t_frame);
		if (t_frame.empty())
		{
			this->m_mtxlock->unlock();
			return NULL;
		}
		cv::resize(t_frame, resizeFrame, dstsize);
		memcpy(this->m_frameBuffer, resizeFrame.data, m_bitDepth * m_height * m_width * sizeof(char));
		ret = this->m_frameBuffer;
	}
	this->m_mtxlock->unlock();
	return ret;
}

OpenCVProvider::~OpenCVProvider()
{
	if(this->m_mtxlock!=nullptr)
	{
		delete this->m_mtxlock;
		this->m_mtxlock = nullptr;
	}
		
	if(this->m_frameBuffer!=nullptr)
	{
		delete[] this->m_frameBuffer;
		this->m_frameBuffer = nullptr;
	}
	if (this->m_cap.isOpened())
	{
		m_cap.release();
	}
	if (this->m_Handle != nullptr)
	{
		delete this->m_Handle;
		this->m_Handle = nullptr;
	}
}

OpenCVProvider* Internal_init()
{
	if (OpenCVProvider::m_Handle == nullptr)
		OpenCVProvider::m_Handle = new OpenCVProvider();
	return OpenCVProvider::m_Handle;
}
