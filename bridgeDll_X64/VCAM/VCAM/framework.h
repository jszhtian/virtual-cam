﻿#pragma once

#define WIN32_LEAN_AND_MEAN             // 从 Windows 头文件中排除极少使用的内容
// Windows 头文件
#include <windows.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>

//Library for COM&DirectShow
#pragma comment(lib, "kernel32")
#pragma comment(lib, "user32")
#pragma comment(lib, "gdi32")
#pragma comment(lib, "advapi32")
#pragma comment(lib, "winmm")
#pragma comment(lib, "ole32")
#pragma comment(lib, "oleaut32")
#pragma comment(lib, "gdiplus")

#ifdef _DEBUG
#pragma comment(lib, "strmbasd")
#pragma comment(lib, "MSVCRTD")
#else
#pragma comment(lib, "strmbase")
#pragma comment(lib, "MSVCRT")
#endif