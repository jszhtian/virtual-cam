#include "pch.h"
#include "VirtualCAMStream.h"




typedef BrigeInterface* (WINAPI* CoInit)();
VirtualCAMStream::VirtualCAMStream(HRESULT* phr, CSource* pFilter):CSourceStream(NAME("VirtualCAM Stream"), phr, pFilter, L"Out")
{
	m_Height = 1080;
	m_Width = 1920;
	m_iFrameNumber = 0;
	//Load Extended Library
	//MessageBoxW(NULL, L"StartLOAD!", L"INFO", MB_OK );
	HMODULE hDLL = LoadLibrary(L"bridgeDll.dll");
	
	if (hDLL != NULL)
	{
		CoInit funcPtr1 = CoInit(GetProcAddress(hDLL, "CoInit"));
		BrigeInterface* Handle = funcPtr1();
		m_Handle = Handle;
	}
	else
	{
		MessageBoxW(NULL, L"Failed to Load Extended Library!", L"Warning", MB_OK | MB_ICONERROR);
	}
	
}


VirtualCAMStream::~VirtualCAMStream()
{
}

HRESULT VirtualCAMStream::DecideBufferSize(IMemAllocator* pAlloc, ALLOCATOR_PROPERTIES* pProperties)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());
	HRESULT hr = NOERROR;
	VIDEOINFO* pvi = (VIDEOINFO*)m_mt.Format();
	pProperties->cBuffers = 1;
	pProperties->cbBuffer = pvi->bmiHeader.biSizeImage;
	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(pProperties, &Actual);
	if (FAILED(hr))
	{
		return hr;
	}
	if (Actual.cbBuffer < pProperties->cbBuffer)
	{
		return E_FAIL;
	}
	return NOERROR;
}
HRESULT VirtualCAMStream::FillBuffer(IMediaSample* pSample)
{
	BYTE* pData;
	long cbData;
	CAutoLock cAutoLockShared(&m_cSharedState);
	pSample->GetPointer(&pData);
	cbData = pSample->GetSize();
	m_Handle->lock();
	auto ptr = m_Handle->get();
	//memcpy(pData, ptr, cbData);
	if(ptr!=NULL)
	{
		for (int i = 0; i < m_Height; i++)
		{
			BYTE* readPos = (byte*)ptr + ((m_Height - i - 1) * m_Width * 3);
			BYTE * writePos = pData + (i * m_Width * 3);
			memcpy(writePos, readPos, m_Width * 3);
		}
	}
	m_Handle->release();
	REFERENCE_TIME rtStart = m_iFrameNumber * m_rtFrameLength;
	REFERENCE_TIME rtStop = rtStart + m_rtFrameLength;
	pSample->SetTime(&rtStart, &rtStop);
	m_iFrameNumber++;
	return NOERROR;
}

HRESULT VirtualCAMStream::SetMediaType(const CMediaType* pMediaType)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());
	HRESULT hr = CSourceStream::SetMediaType(pMediaType);
	if (SUCCEEDED(hr))
	{
		VIDEOINFO* pvi = (VIDEOINFO*)m_mt.Format();
		if (pvi == NULL)
			return E_UNEXPECTED;
		switch (pvi->bmiHeader.biBitCount)
		{
		case 24:    // RGB24
			// Save the current media type and bit depth
			m_MediaType = *pMediaType;
			m_nCurrentBitDepth = pvi->bmiHeader.biBitCount;
			hr = S_OK;
			break;

		default:
			// We should never agree any other media types
			ASSERT(FALSE);
			hr = E_INVALIDARG;
			break;
		}
	}
}

HRESULT VirtualCAMStream::CheckMediaType(const CMediaType* pMediaType)
{
	if (*(pMediaType->Type()) != MEDIATYPE_Video)
		return E_INVALIDARG;
	const GUID* SubType = pMediaType->Subtype();
	if (SubType == NULL)
		return E_INVALIDARG;
	if ((*SubType != MEDIASUBTYPE_RGB24))
	{
		return E_INVALIDARG;
	}
	VIDEOINFO* pvi = (VIDEOINFO*)pMediaType->Format();

	if (pvi == NULL)
		return E_INVALIDARG;
	if (pvi->bmiHeader.biWidth != m_Width ||abs(pvi->bmiHeader.biHeight) != m_Height)
	{
		return E_INVALIDARG;
	}
	if (pvi->bmiHeader.biHeight < 0)
		return E_INVALIDARG;

	return S_OK;

}

HRESULT VirtualCAMStream::GetMediaType(int iPosition, CMediaType* pmt)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());
	if (iPosition < 0)
		return E_INVALIDARG;
	if (iPosition != 0)
		return VFW_S_NO_MORE_ITEMS;
	VIDEOINFO* pvi = (VIDEOINFO*)pmt->AllocFormatBuffer(sizeof(VIDEOINFO));
	if (NULL == pvi)
		return(E_OUTOFMEMORY);
	ZeroMemory(pvi, sizeof(VIDEOINFO));
	switch (iPosition)
	{
	case 0:
		pvi->bmiHeader.biCompression = BI_RGB;
		pvi->bmiHeader.biBitCount = 24;
		break;
	default:
		return VFW_S_NO_MORE_ITEMS;
	}
	pvi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pvi->bmiHeader.biWidth = m_Width;
	pvi->bmiHeader.biHeight = m_Height;
	pvi->bmiHeader.biPlanes = 1;
	pvi->bmiHeader.biSizeImage = GetBitmapSize(&pvi->bmiHeader);
	pvi->bmiHeader.biClrImportant = 0;
	SetRectEmpty(&(pvi->rcSource));
	SetRectEmpty(&(pvi->rcTarget));
	pmt->SetType(&MEDIATYPE_Video);
	pmt->SetFormatType(&FORMAT_VideoInfo);
	pmt->SetTemporalCompression(FALSE);
	const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
	pmt->SetSubtype(&SubTypeGUID);
	pmt->SetSampleSize(pvi->bmiHeader.biSizeImage);
	return NOERROR;

}
