#pragma once
#include "Include/source.h"
#include "brigeInterface.h"
const REFERENCE_TIME FPS_Val = UNITS / 30;
class VirtualCAMStream :
	public CSourceStream
{
protected:
	int m_Width;
	int m_Height;
	CMediaType m_MediaType;
	int m_nCurrentBitDepth;
	CCritSec m_cSharedState;
	int m_iFrameNumber;
	REFERENCE_TIME m_rtLastTime;
	const REFERENCE_TIME m_rtFrameLength= FPS_Val;
	BrigeInterface* m_Handle;

public:
	VirtualCAMStream(HRESULT*, CSource*);
	~VirtualCAMStream();
	HRESULT DecideBufferSize(IMemAllocator*, ALLOCATOR_PROPERTIES*);
	HRESULT FillBuffer(IMediaSample*);
	HRESULT SetMediaType(const CMediaType*);
	HRESULT CheckMediaType(const CMediaType*);
	HRESULT GetMediaType(int iPosition, CMediaType*);
	STDMETHODIMP Notify(IBaseFilter* pSelf, Quality q)
	{
		return E_FAIL;
	}
};

