#include "pch.h"
#include "MT_SharedMemory.h"


MT_ShareMemory::MT_ShareMemory(size_t size)
{
	m_SbufSize = size;
	m_mtx = new std::mutex;
	m_pBuf = new char[m_SbufSize];
}


MT_ShareMemory::~MT_ShareMemory()
{
	delete[] m_pBuf;
	delete m_mtx;
}

bool MT_ShareMemory::Read(char* Dst, size_t size)
{
	if (size > m_SbufSize)
		return false;
	m_mtx->lock();
	memcpy(Dst, m_pBuf, size);
	m_mtx->unlock();
	return true;
}

bool MT_ShareMemory::Write(char* Src, size_t size)
{
	if (size > m_SbufSize)
		return false;
	m_mtx->lock();
	memcpy(m_pBuf,Src, size);
	m_mtx->unlock();
	return true;
}
