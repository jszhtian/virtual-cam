#pragma once
#include <mutex>
class MT_ShareMemory
{
public:
	MT_ShareMemory(size_t);
	~MT_ShareMemory();
	bool Read(char*  ,size_t );
	bool Write(char* , size_t );
private:
	char* m_pBuf;
	std::mutex* m_mtx;
	size_t m_SbufSize;
};

