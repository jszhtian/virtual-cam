#include "pch.h"
#include "CSourceVirtualCAM.h"
#include "VirtualCAMStream.h"


CUnknown* __stdcall CSourceVirtualCAM::CreateInstance(IUnknown* pUnk, HRESULT* phr)
{
	CSourceVirtualCAM* pNewFilter = new CSourceVirtualCAM(pUnk, phr);
	if (phr)
	{
		if (pNewFilter == NULL)
			* phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}
	return pNewFilter;
}


CSourceVirtualCAM::CSourceVirtualCAM(IUnknown* pUnk, HRESULT* phr):CSource(NAME("Virtual CAM Source"),pUnk,CLSID_VirtualCAM)
{
	m_pPin = new VirtualCAMStream(phr, this);
	if (phr)
	{
		if (m_pPin == NULL)
			* phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}
}

CSourceVirtualCAM::~CSourceVirtualCAM()
{
	delete m_pPin;
}
