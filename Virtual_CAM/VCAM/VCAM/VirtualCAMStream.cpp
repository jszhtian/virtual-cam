#include "pch.h"
#include "VirtualCAMStream.h"
#include <gdiplus.h>


void FillBuf(LPVOID para)
{
	char* tmp = new char[1920 * 1080 * 3];
	MT_ShareMemory* mem = (MT_ShareMemory*)para;
	while(1)
	{
		for (int i = 0; i < 1920 * 1080 * 3; i++)
			tmp[i] = rand();
		mem->Write(tmp, 1920 * 1080 * 3);
		Sleep(30);
	}
	
}

VirtualCAMStream::VirtualCAMStream(HRESULT* phr, CSource* pFilter):CSourceStream(NAME("VirtualCAM Stream"), phr, pFilter, L"Out")
{
	m_Height = 1080;
	m_Width = 1920;
	m_iFrameNumber = 0;
	m_MTSharedMem = new MT_ShareMemory(m_Height * m_Width * 3);
	CreateThread(0, 0, (LPTHREAD_START_ROUTINE)FillBuf, (PVOID)m_MTSharedMem,0,0);
}


VirtualCAMStream::~VirtualCAMStream()
{
	delete m_MTSharedMem;
}

//GDI+ Based BMP Loader

/*
 char* PicPtr = nullptr;
using namespace Gdiplus;
void GetX(int bitdepth)
{
	Gdiplus::GdiplusStartupInput gdiplusstartupinput;
	ULONG_PTR gdiplustoken;
	Gdiplus::GdiplusStartup(&gdiplustoken, &gdiplusstartupinput, NULL);
	Gdiplus::Bitmap* bmp = new Gdiplus::Bitmap(L"E:\\jszht\\desktop\\DirectShow\\vcam\\Release\\test.bmp");
	if (bmp == NULL)
	{
		MessageBoxW(NULL, L"INFO1", L"GDI FAIL", MB_OK);
	}
	UINT height = bmp->GetHeight();
	UINT width = bmp->GetWidth();
	Color color;
	switch (bitdepth)
	{
	case 24:
		PicPtr = new char[1920 * 1080 * 3];
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
			{
				bmp->GetPixel(x, y, &color);
				y = height - 1 - y;
				PicPtr[(x + y * width) * 3 + 2] = color.GetRed();
				PicPtr[(x + y * width) * 3 + 1] = color.GetGreen();
				PicPtr[(x + y * width) * 3 + 0] = color.GetBlue();
			}
		break;
		break;
	case 32:
		PicPtr = new char[1920 * 1080 * 4];
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
			{
				bmp->GetPixel(x, y, &color);
				y = height - 1 - y;
				PicPtr[(x + y * width) * 4 + 3] = color.GetAlpha();
				PicPtr[(x + y * width) * 4 + 2] = color.GetRed();
				PicPtr[(x + y * width) * 4 + 1] = color.GetGreen();
				PicPtr[(x + y * width) * 4 + 0] = color.GetBlue();
			}
		break;
	default:
		MessageBoxW(NULL, L"Not Support BitDepth", L"INFO", MB_OK);
	}
	
	
	
}
 */

HRESULT VirtualCAMStream::DecideBufferSize(IMemAllocator* pAlloc, ALLOCATOR_PROPERTIES* pProperties)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());
	HRESULT hr = NOERROR;
	VIDEOINFO* pvi = (VIDEOINFO*)m_mt.Format();
	pProperties->cBuffers = 1;
	pProperties->cbBuffer = pvi->bmiHeader.biSizeImage;
	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(pProperties, &Actual);
	if (FAILED(hr))
	{
		return hr;
	}
	if (Actual.cbBuffer < pProperties->cbBuffer)
	{
		return E_FAIL;
	}
	return NOERROR;
}
HRESULT VirtualCAMStream::FillBuffer(IMediaSample* pSample)
{
	BYTE* pData;
	long cbData;
	CAutoLock cAutoLockShared(&m_cSharedState);
	pSample->GetPointer(&pData);
	cbData = pSample->GetSize();
	/*
	 
	 if (!PicPtr)
	{
		GetX(m_nCurrentBitDepth);
	}
	for (int i = 0; i < cbData; i++)
	{
		pData[i] = PicPtr[i];//B
	}
	 */
	//for (int i = 0; i < cbData; i++)
	//{
	//	pData[i] = rand()%0xFF;//B
	//}
	m_MTSharedMem->Read((char*)pData, cbData);
	REFERENCE_TIME rtStart = m_iFrameNumber * m_rtFrameLength;
	REFERENCE_TIME rtStop = rtStart + m_rtFrameLength;
	pSample->SetTime(&rtStart, &rtStop);
	m_iFrameNumber++;
	return NOERROR;
}

HRESULT VirtualCAMStream::SetMediaType(const CMediaType* pMediaType)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());
	HRESULT hr = CSourceStream::SetMediaType(pMediaType);
	if (SUCCEEDED(hr))
	{
		VIDEOINFO* pvi = (VIDEOINFO*)m_mt.Format();
		if (pvi == NULL)
			return E_UNEXPECTED;
		switch (pvi->bmiHeader.biBitCount)
		{
		case 24:    // RGB24
			// Save the current media type and bit depth
			m_MediaType = *pMediaType;
			m_nCurrentBitDepth = pvi->bmiHeader.biBitCount;
			hr = S_OK;
			break;

		default:
			// We should never agree any other media types
			ASSERT(FALSE);
			hr = E_INVALIDARG;
			break;
		}
	}
}

HRESULT VirtualCAMStream::CheckMediaType(const CMediaType* pMediaType)
{
	if (*(pMediaType->Type()) != MEDIATYPE_Video)
		return E_INVALIDARG;
	const GUID* SubType = pMediaType->Subtype();
	if (SubType == NULL)
		return E_INVALIDARG;
	if ((*SubType != MEDIASUBTYPE_RGB24))
	{
		return E_INVALIDARG;
	}
	VIDEOINFO* pvi = (VIDEOINFO*)pMediaType->Format();

	if (pvi == NULL)
		return E_INVALIDARG;
	if (pvi->bmiHeader.biWidth != m_Width ||abs(pvi->bmiHeader.biHeight) != m_Height)
	{
		return E_INVALIDARG;
	}
	if (pvi->bmiHeader.biHeight < 0)
		return E_INVALIDARG;

	return S_OK;

}

HRESULT VirtualCAMStream::GetMediaType(int iPosition, CMediaType* pmt)
{
	CAutoLock cAutoLock(m_pFilter->pStateLock());
	if (iPosition < 0)
		return E_INVALIDARG;
	if (iPosition != 0)
		return VFW_S_NO_MORE_ITEMS;
	VIDEOINFO* pvi = (VIDEOINFO*)pmt->AllocFormatBuffer(sizeof(VIDEOINFO));
	if (NULL == pvi)
		return(E_OUTOFMEMORY);
	ZeroMemory(pvi, sizeof(VIDEOINFO));
	switch (iPosition)
	{
	case 0:
		pvi->bmiHeader.biCompression = BI_RGB;
		pvi->bmiHeader.biBitCount = 24;
		break;
	default:
		return VFW_S_NO_MORE_ITEMS;
	}
	pvi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pvi->bmiHeader.biWidth = m_Width;
	pvi->bmiHeader.biHeight = m_Height;
	pvi->bmiHeader.biPlanes = 1;
	pvi->bmiHeader.biSizeImage = GetBitmapSize(&pvi->bmiHeader);
	pvi->bmiHeader.biClrImportant = 0;
	SetRectEmpty(&(pvi->rcSource));
	SetRectEmpty(&(pvi->rcTarget));
	pmt->SetType(&MEDIATYPE_Video);
	pmt->SetFormatType(&FORMAT_VideoInfo);
	pmt->SetTemporalCompression(FALSE);
	const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
	pmt->SetSubtype(&SubTypeGUID);
	pmt->SetSampleSize(pvi->bmiHeader.biSizeImage);
	return NOERROR;

}
