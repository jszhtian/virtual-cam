﻿// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "pch.h"
#include "CSourceVirtualCAM.h"



const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
	&MEDIATYPE_Video,       // Major type
	&MEDIASUBTYPE_NULL      // Minor type
};
const AMOVIESETUP_PIN sudOutputPinVirtualCAM =
{
	L"Output",      // Obsolete, not used.
	FALSE,          // Is this pin rendered?
	TRUE,           // Is it an output pin?
	FALSE,          // Can the filter create zero instances?
	FALSE,          // Does the filter create multiple instances?
	&CLSID_NULL,    // Obsolete.
	NULL,           // Obsolete.
	1,              // Number of media types.
	&sudOpPinTypes  // Pointer to media types.
};

const AMOVIESETUP_FILTER sudPushSourceBitmap =
{
	&CLSID_VirtualCAM,// Filter CLSID
	L"VirtualCAM",        // String name
	MERIT_DO_NOT_USE,       // Filter merit
	1,                      // Number pins
	& sudOutputPinVirtualCAM     // Pin details
};

CFactoryTemplate g_Templates[] =
{
	{
		L"Virtual CAM",
		& CLSID_VirtualCAM,
		CSourceVirtualCAM::CreateInstance,
		NULL,
		& sudPushSourceBitmap
	},

};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);

STDAPI DllRegisterServer()
{
	return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
	return AMovieDllRegisterServer2(FALSE);
}

extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD  dwReason,
	LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

