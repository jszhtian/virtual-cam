#pragma once
#include "Include/source.h"
#include "VirtualCAMStream.h"
class CSourceVirtualCAM:public CSource
{
public:
	static CUnknown* WINAPI CreateInstance(IUnknown*, HRESULT*);
private:
	VirtualCAMStream* m_pPin;
	CSourceVirtualCAM(IUnknown* pUnk, HRESULT* phr);
	~CSourceVirtualCAM();
};

