﻿// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "pch.h"
#include "CSourceVirtualCAM.h"



const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
	&MEDIATYPE_Video,       // Major type
	&MEDIASUBTYPE_NULL      // Minor type
};
const AMOVIESETUP_PIN sudOutputPinVirtualCAM =
{
	L"Output",      // Obsolete, not used.
	FALSE,          // Is this pin rendered?
	TRUE,           // Is it an output pin?
	FALSE,          // Can the filter create zero instances?
	FALSE,          // Does the filter create multiple instances?
	&CLSID_NULL,    // Obsolete.
	NULL,           // Obsolete.
	1,              // Number of media types.
	&sudOpPinTypes  // Pointer to media types.
};

const AMOVIESETUP_FILTER sudPushSourceBitmap =
{
	&CLSID_VirtualCAM,// Filter CLSID
	L"Virtual CAM",        // String name
	MERIT_DO_NOT_USE,       // Filter merit
	1,                      // Number pins
	& sudOutputPinVirtualCAM     // Pin details
};

CFactoryTemplate g_Templates[] =
{
	{
		L"Virtual CAM",
		& CLSID_VirtualCAM,
		CSourceVirtualCAM::CreateInstance,
		NULL,
		& sudPushSourceBitmap
	},

};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);

REGFILTER2 rf2FilterReg = {
	1,              // Version 1 (no pin mediums or pin category).
	MERIT_DO_NOT_USE,   // Merit.
	1,              // Number of pins.
	&sudOutputPinVirtualCAM        // Pointer to pin information.
};

STDAPI DllRegisterServer()
{
	HRESULT hr;
	IFilterMapper2* pFM2 = NULL;

	hr = AMovieDllRegisterServer2(TRUE);
	if (FAILED(hr))
		return hr;

	hr = CoCreateInstance(CLSID_FilterMapper2, NULL, CLSCTX_INPROC_SERVER,
		IID_IFilterMapper2, (void**)& pFM2);

	if (FAILED(hr))
		return hr;

	hr = pFM2->RegisterFilter(
		CLSID_VirtualCAM,                // Filter CLSID. 
		L"Virtual CAM",                       // Filter name.
		NULL,                            // Device moniker. 
		&CLSID_VideoInputDeviceCategory,  // Video compressor category.
		L"Virtual CAM",                       // Instance data.
		&rf2FilterReg                    // Pointer to filter information.
	);
	pFM2->Release();
	return hr;
}

STDAPI DllUnregisterServer()
{
	HRESULT hr;
	IFilterMapper2* pFM2 = NULL;

	hr = AMovieDllRegisterServer2(FALSE);
	if (FAILED(hr))
		return hr;

	hr = CoCreateInstance(CLSID_FilterMapper2, NULL, CLSCTX_INPROC_SERVER,
		IID_IFilterMapper2, (void**)& pFM2);

	if (FAILED(hr))
		return hr;

	hr = pFM2->UnregisterFilter(&CLSID_VideoInputDeviceCategory,
		L"Virtual CAM", CLSID_VirtualCAM);

	pFM2->Release();
	return hr;
}
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD  dwReason,
	LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

