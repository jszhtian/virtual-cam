﻿// bridgeTester.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
#include "brigeInterface.h"
#include <windows.h>
#include <iostream>
typedef BrigeInterface*(WINAPI* CoInit)();
int main()
{
	HMODULE hDLL = LoadLibrary(L"bridgeDll.dll");
	if (hDLL != NULL)
	{
		CoInit funcPtr1 = CoInit(GetProcAddress(hDLL, "CoInit"));
		auto Handle = funcPtr1();
		auto res = Handle->lock();
		if (res == true)
			std::cout << "LOCK WORK!\n";
		Handle->get();
		Handle->get();
		Handle->get();
		Handle->get();
		Handle->get();
		auto res2 = Handle->get();
		if (res2 != NULL)
			std::cout << "GET WORK!\n";
		auto res3 = Handle->release();
		if (res3 == true)
			std::cout << "RELEASE WORK!\n";
		auto res4 = Handle->uninit();
		if (res3 == true)
			std::cout << "UNINIT WORK!\n";
		delete Handle;


	}
    std::cout << "Hello World!\n"; 
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
