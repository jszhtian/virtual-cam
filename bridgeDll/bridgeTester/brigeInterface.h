#pragma once

class BrigeInterface
{
public:
	virtual char* get() = 0;
	virtual bool release() = 0;
	virtual bool lock() = 0;
	virtual bool init() = 0;
	virtual bool uninit() = 0;
protected:
	bool m_mtx=false;
};