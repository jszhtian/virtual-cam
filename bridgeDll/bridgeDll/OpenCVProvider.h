#pragma once
#include "brigeInterface.h"
#include "opencv2/opencv.hpp"
#include <mutex>
//for OpenCV2
#pragma comment(lib, "opencv_calib3d410.lib")
#pragma comment(lib, "opencv_core410.lib")
#pragma comment(lib, "opencv_dnn410.lib")
#pragma comment(lib, "opencv_features2d410.lib")
#pragma comment(lib, "opencv_flann410.lib")
#pragma comment(lib, "opencv_gapi410.lib")
#pragma comment(lib, "opencv_highgui410.lib")
#pragma comment(lib, "opencv_imgcodecs410.lib")
#pragma comment(lib, "opencv_imgproc410.lib")
#pragma comment(lib, "opencv_ml410.lib")
#pragma comment(lib, "opencv_objdetect410.lib")
#pragma comment(lib, "opencv_photo410.lib")
#pragma comment(lib, "opencv_stitching410.lib")
#pragma comment(lib, "opencv_video410.lib")
#pragma comment(lib, "opencv_videoio410.lib")
class OpenCVProvider :
	public BrigeInterface
{
public:
	OpenCVProvider();
	static OpenCVProvider* m_Handle;
	bool init();
	bool lock();
	bool release();
	bool uninit();
	char* get();
	~OpenCVProvider();
	
private:
	char* m_frameBuffer = nullptr;
	std::mutex* m_mtxlock=nullptr;
	int m_width = 1920;
	int m_height = 1080;
	int m_bitDepth = 3;
	cv::VideoCapture m_cap;
};

OpenCVProvider* Internal_init();